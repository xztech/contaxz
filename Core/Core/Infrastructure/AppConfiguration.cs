using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Core
{
    public static class AppConfiguration
    {
        public static string ApiUrl => "https://localhost:5500";

        public static async Task Initialize()
        {
            StoreService.SavePinkey($"Email");
            if (Preferences.ContainsKey(Constants.Storekey))
            {
                StoreService.Start(
                     await Task.FromResult(Preferences.Get(Constants.Storekey, string.Empty))
                );
            }
        }
    }
}