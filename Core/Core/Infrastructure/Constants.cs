﻿using System;
using System.IO;
using System.Security;

namespace Core
{
    public static class Constants
    {
        public static string Database => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "database.db");
        public static string Images => $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}/localImages";
        public static string Audios => $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}/localAudios";

        public const string PinKey = "pk";
        public const string Storekey = "sk";
    }
}