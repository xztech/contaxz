﻿using Prism.Mvvm;
using Prism.Navigation;
using System.Threading.Tasks;

namespace Core
{
    public abstract class BaseViewModel : BindableBase, INavigationAware, IDestructible
    {
        protected readonly INavigationService _navigationService;

        private string _title;
        public string Title
        {
            get => _title;
            protected set => SetProperty(ref _title, value);
        }
        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (value == _isBusy)
                    return;

                //if (value)
                //    _loadingPageService.ShowLoadingPage();
                //else
                //    _loadingPageService.HideLoadingPage();

                SetProperty(ref _isBusy, value);
            }
        }

        protected BaseViewModel(INavigationService navigationService)
            => _navigationService = navigationService;

        protected BaseViewModel(INavigationService navigationService, string title) : this(navigationService)
            => Title = title;

        #region Init

        protected virtual Task InitializeAsync()
            => Task.FromResult(true);
        #endregion

        //Quando você estiver saindo da página .. e antes de navegar dela ..        
        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        //Será disparado -invoked- quando você chegar à página.
        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
        }

        public virtual void Destroy()
        {
        }
    }
}

