﻿using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Core
{
    public partial class DashboardViewModel : BaseViewModel
    {
        #region Prop
        public ObservableCollection<Transaction> _accounts;
        public ObservableCollection<Transaction> Accounts
        {
            get => _accounts;
            set => SetProperty(ref _accounts, value);
        }

        public double _totalBalance;
        public double TotalBalance
        {
            get => _totalBalance;
            set => SetProperty(ref _totalBalance, value);
        }
        #endregion

        #region Command
        public ICommand AddCommand { get; }
        #endregion

        #region Init
        public DashboardViewModel(INavigationService navigationService) : base(navigationService)
        {
            Accounts = new ObservableCollection<Transaction>();
            AddCommand = new Command(async () => await ExecuteAddCommandAsync());
        }
        #endregion
        async Task ExecuteAddCommandAsync()
        {
            await _navigationService.NavigateAsync(nameof(NewTransactionPage),null, true, true);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            TotalBalance = 0;
            var collection = StoreService.Instance.GetCollection<Transaction>();
            var list = collection.FindAll();
            Accounts = new ObservableCollection<Transaction>();
            foreach (var item in list)
            {
                Accounts.Add(item);
                TotalBalance += item.Value;
            }
            
            //{
            //    new Transaction{Value = 200.00, Name="Bradesco"},
            //    new Transaction{Value = 2000.00, Name="Inter"},
            //    new Transaction{Value = 400.00, Name="Caixa"},
            //};
            
            base.OnNavigatedTo(parameters);
        }
    }
}