﻿using Prism.Navigation;

namespace Core
{
    public class MainViewModel : BaseViewModel
    {

        public MainViewModel(INavigationService navigationService) : base(navigationService)
        {
        }
    }
}