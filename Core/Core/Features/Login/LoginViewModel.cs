﻿using Prism.Navigation;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Core
{
    public class LoginViewModel : BaseViewModel
    {
        #region Prop
        string _phone;
        public string Phone { get => _phone; set => SetProperty(ref _phone, value); }
        string _password;
        public string Password { get => _password; set => SetProperty(ref _password, value); }
        #endregion

        #region Command
        public ICommand LoginCommand { get; }
        public ICommand SignupCommand { get; }
        #endregion

        #region Init
        public LoginViewModel(INavigationService navigationService) : base(navigationService)
        {

            LoginCommand = new Command(async () => await ExecuteLoginCommandAsync());
            SignupCommand = new Command(async () => await ExecuteSignupCommandAsync());
        }
        #endregion

        #region Execute Command

        async Task ExecuteSignupCommandAsync()
        {
            throw new NotImplementedException();
        }

        async Task ExecuteLoginCommandAsync()
        {
            await _navigationService.NavigateAsync(nameof(DashboardPage));
        }
        #endregion



    }
}