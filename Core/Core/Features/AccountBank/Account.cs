﻿namespace Core
{
    public class Account
    {
        public string Name { get; set; }
        public double Balance { get; set; }
        public AccountType AccountType { get; set; }
        public string Icon { get; set; }
    }
    public enum AccountType
    {
        Checking,
        Investiment,
        Savings
    }
}
