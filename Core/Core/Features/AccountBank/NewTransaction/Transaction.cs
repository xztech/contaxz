﻿using LiteDB;

namespace Core
{
    public class Transaction
    {
        public string Name { get; set; }
        public double Value { get; set; }
        //public TransactionType Type { get; set; }
    }
    public enum TransactionType
    {
        Revenue,
        Expense
    }
}
