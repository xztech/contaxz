﻿using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Core
{
    public class NewTransactionViewModel : BaseViewModel
    {
       
        #region Prop
        public double _value;
        public double Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public double _totalBalance;
        public double TotalBalance
        {
            get => _totalBalance;
            set => SetProperty(ref _totalBalance, value);
        }
        #endregion

        #region Command
        public ICommand CloseCommand { get; }
        public ICommand SaveCommand { get; }

        #endregion

        #region Init

        public NewTransactionViewModel(INavigationService navigationService ) : base(navigationService)
        {
            CloseCommand = new Command(async () => await ExecuteCloseCommandAsync());
            SaveCommand = new Command(async () => await ExecuteSaveCommandAsync());
        }

        private async Task ExecuteSaveCommandAsync()
        {
            var transaction = new Transaction { Name = Name, Value = Value };
            var collection = StoreService.Instance.GetCollection<Transaction>();
            collection.Upsert(transaction);
            StoreService.Instance.Commit();

            var list = collection.FindAll();
            await ExecuteCloseCommandAsync();
        }

        #endregion
        #region Execute Command
        private async Task ExecuteCloseCommandAsync()
        {
            await _navigationService.GoBackAsync(null, true,true);
        }
        #endregion
    }
}