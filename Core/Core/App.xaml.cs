﻿using Xamarin.Forms;
using System.Diagnostics;
using Prism.Ioc;

namespace Core
{
    public partial class App
    {
        public App()
        {
        }


        protected override async void OnInitialized()
        {
            InitializeComponent();
            await AppConfiguration.Initialize();
            var result = await NavigationService.NavigateAsync($"{nameof(LoginPage)}");
            if (!result.Success)
                Debugger.Break();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //Pages ViewModels
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, NewTransactionViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginViewModel>();
            containerRegistry.RegisterForNavigation<DashboardPage, DashboardViewModel>();
            containerRegistry.RegisterForNavigation<NewTransactionPage, NewTransactionViewModel>();

            //Services
            containerRegistry.RegisterSingleton<IDialogService, DialogService>();
        }

    }
}
